//
//  Constants.swift
//  Vibro
//
//  Created by Контрабаев Артур on 16.09.2021.
//

import UIKit

struct K {
    static let SCREEN_SIZE = UIScreen.main.bounds
    static let turnOnVibroCell = "turnOnVibroCell"
    static let intensityCell = "intensityCell"
    static let speedCell = "speedCell"
    static let modesCell = "modesCell"
    static let modeCell = "modeCell"
    
    static let startSpeed: Float = 1.5
    static let startMode = "Waves"
    static let startIntensity = K.Intensity.soft
    
    struct Colors {
        static let redBright = #colorLiteral(red: 0.7333333333, green: 0.1843137255, blue: 0.2352941176, alpha: 1)
        static let redDark = #colorLiteral(red: 0.4666666667, green: 0.1529411765, blue: 0.1803921569, alpha: 1)
        static let pinkStandard = #colorLiteral(red: 1, green: 0.2196078431, blue: 0.2901960784, alpha: 1)
        static let greyMiddle = #colorLiteral(red: 0.3921568627, green: 0.3921568627, blue: 0.3921568627, alpha: 1)
        static let blackStandard = #colorLiteral(red: 0.1098039216, green: 0.1098039216, blue: 0.1098039216, alpha: 1)
        static let blackLighter = #colorLiteral(red: 0.137254902, green: 0.137254902, blue: 0.137254902, alpha: 1)
    }
 
    enum Intensity: String {
        case soft = "Soft"
        case medium = "Medium"
        case hard = "Hard"
    }
    
    static let modes: [(name: String,image: UIImage)] = [
        ("Waves",#imageLiteral(resourceName: "mode_waves")),
        ("Breeze",#imageLiteral(resourceName: "mode_breeze")),
        ("Monsoon",#imageLiteral(resourceName: "mode_monsoon")),
        ("Hurricane",#imageLiteral(resourceName: "mode_hurricane")),
        ("Volcano",#imageLiteral(resourceName: "mode_volcano mde")),
        ("Tsunami",#imageLiteral(resourceName: "mode_tsunami")),
        ("Typhoon",#imageLiteral(resourceName: "mode_typhoon")),
        ("Storm",#imageLiteral(resourceName: "mode_storm")),
        ("Lava",#imageLiteral(resourceName: "mode_lava")),
        ("Billow",#imageLiteral(resourceName: "mode_billow")),
        ("Quake",#imageLiteral(resourceName: "mode_quake")),
        ("Tornado",#imageLiteral(resourceName: "mode_tornado")),
    ]
    
    static let modesPattern: [String: [Int]] = [
        "Waves": [20, 100, 20, 200, 20, 300, 20, 200],
        "Breeze": [20, 100, 20, 300, 20, 200],
        "Monsoon": [20, 300, 20, 250, 40, 100],
        "Hurricane": [20, 150, 20, 200, 20, 350],
        "Volcano": [20, 200, 40, 300, 60, 400],
        "Tsunami": [20, 100, 20, 500, 20, 200],
        "Typhoon": [20, 250, 20, 500, 20, 100],
        "Storm": [20, 400, 40, 100, 20, 250],
        "Lava": [20, 100, 40, 100, 60, 100],
        "Billow": [40, 200, 60, 200, 20, 100],
        "Quake": [20, 100, 60, 150, 40, 500],
        "Tornado": [20, 500, 20, 600, 20, 700, 20, 800]
    ]
    
    static let supportEmail = "support@kongri.tech"
    static let termsLink = "https://docs.google.com/document/d/1jDwvRCP3Eos8BO792ysRJb7nO0QQbBv13E8H6HN65CE/edit?usp=sharing"
    static let policyLink = "https://docs.google.com/document/d/1xa0uH-IBwmwSf9MsLLlAJ6p-a42ksmmJskjANdrv1N0/edit?usp=sharing"
 }
