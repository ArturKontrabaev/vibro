//
//  intensityswift
//  Vibro
//
//  Created by Контрабаев Артур on 16.09.2021.
//

import UIKit
protocol IntensityCellDelegate: AnyObject {
    func setNewIntensity(selectedIntensity: K.Intensity)
}

class intensityCell: UITableViewCell {

    @IBOutlet weak var intensityLabel: UILabel!
    @IBOutlet weak var softButton: UIButton!
    @IBOutlet weak var mediumButton: UIButton!
    @IBOutlet weak var hardButton: UIButton!
    
    weak var delegate: IntensityCellDelegate?
    
    @IBAction func intensityButtonPressed(_ sender: UIButton) {
        if sender == softButton {
            delegate?.setNewIntensity(selectedIntensity: .soft)
            softButton.backgroundColor = K.Colors.redBright
            mediumButton.backgroundColor = K.Colors.redDark
            hardButton.backgroundColor = K.Colors.redDark
        } else if sender == mediumButton {
            delegate?.setNewIntensity(selectedIntensity: .medium)
            softButton.backgroundColor = K.Colors.redDark
            mediumButton.backgroundColor = K.Colors.redBright
            hardButton.backgroundColor = K.Colors.redDark
        } else {
            delegate?.setNewIntensity(selectedIntensity: .hard)
            softButton.backgroundColor = K.Colors.redDark
            mediumButton.backgroundColor = K.Colors.redDark
            hardButton.backgroundColor = K.Colors.redBright
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        backgroundColor = K.Colors.blackStandard
        selectionStyle = .none
        var chosenIntensity: K.Intensity = .soft
        
        if let intensity = UserDefaults.standard.string(forKey: "chosenIntensity"){
            switch intensity{
            case K.Intensity.soft.rawValue:
                chosenIntensity = .soft
            case K.Intensity.medium.rawValue:
                chosenIntensity = .medium
            case K.Intensity.hard.rawValue:
                chosenIntensity = .hard
            default:
                chosenIntensity = .soft
            }
            delegate?.setNewIntensity(selectedIntensity: chosenIntensity)
        }
        
        intensityLabel.text = "INTENSITY"
        intensityLabel.textColor = #colorLiteral(red: 0.8509803922, green: 0.8509803922, blue: 0.8509803922, alpha: 1)
        intensityLabel.font = UIFont(name: "SFProDisplay-Regular", size: 16)
        
        softButton.setTitle("Soft", for: .normal)
        softButton.layer.masksToBounds = true
        softButton.layer.cornerRadius = 20
        softButton.backgroundColor = K.Colors.redDark
        softButton.titleLabel?.font = UIFont (name: "SFProDisplay-Regular", size: 14)
        softButton.setTitleColor(.white, for: .normal)
       
        mediumButton.setTitle("Medium", for: .normal)
        mediumButton.layer.masksToBounds = true
        mediumButton.layer.cornerRadius = 20
        mediumButton.backgroundColor = K.Colors.redDark
        mediumButton.titleLabel?.font = UIFont (name: "SFProDisplay-Regular", size: 14)
        mediumButton.setTitleColor(.white, for: .normal)
        
        hardButton.setTitle("Hard", for: .normal)
        hardButton.layer.masksToBounds = true
        hardButton.layer.cornerRadius = 20
        hardButton.backgroundColor = K.Colors.redDark
        hardButton.titleLabel?.font = UIFont (name: "SFProDisplay-Regular", size: 14)
        hardButton.setTitleColor(.white, for: .normal)
        
        switch chosenIntensity {
        case .soft:
            softButton.backgroundColor = K.Colors.redBright
        case .medium:
            mediumButton.backgroundColor = K.Colors.redBright
        case .hard:
            hardButton.backgroundColor = K.Colors.redBright
        }
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
