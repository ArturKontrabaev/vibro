//
//  turnOnVibroswift
//  Vibro
//
//  Created by Контрабаев Артур on 16.09.2021.
//

import UIKit

protocol turnOnVibroCellDelegate: AnyObject {
    func turnOnButtonPressed(vibroTurnedOn: Bool)
}

class turnOnVibroCell: UITableViewCell {
  
    @IBOutlet weak var selectedModeLabel: UILabel!
    @IBOutlet weak var pressButtonLabel: UILabel!
    @IBOutlet weak var pressButtonLabel2: UILabel!
    @IBOutlet weak var turnOnButton: UIButton!
    @IBOutlet weak var vibroImage: UIImageView!
    @IBOutlet weak var vibroImage2: UIImageView!
    @IBOutlet weak var doesntVibrateButton: UIButton!
    
    
    weak var delegate:turnOnVibroCellDelegate?
    
    var vibroON: Bool = false
    
    @IBAction func turnOnButtonPressed(_ sender: UIButton) {
        vibroON = !vibroON
        
        if vibroON {
            turnOnButton.setImage(#imageLiteral(resourceName: "pauseButton"), for: .normal)
            pressButtonLabel2.text = "to pause massage"
        } else {
            turnOnButton.setImage(#imageLiteral(resourceName: "turnOnButton"), for: .normal)
            pressButtonLabel2.text = "to start massage"
        }
        delegate?.turnOnButtonPressed(vibroTurnedOn: vibroON)
    }

    @IBAction func doesntVibrateButtonPressed(_ sender: UIButton) {
        let doesntVibrateVC = DoesntVibrateAlert.loadFromNib()
        doesntVibrateVC.modalPresentationStyle = .automatic//.overFullScreen
        UIApplication.topViewController()?.present(doesntVibrateVC, animated: true, completion: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        backgroundColor = K.Colors.blackStandard
        selectionStyle = .none
        
        let mode = UserDefaults.standard.string(forKey: "chosenMode") ?? K.startMode
        selectedModeLabel.text = mode
        selectedModeLabel.textColor = .white
        selectedModeLabel.font = UIFont(name: "SFProDisplay-Semibold", size: 34)
        
        pressButtonLabel.text = "Press the button"
        pressButtonLabel.textColor = #colorLiteral(red: 0.7333333333, green: 0.337254902, blue: 0.3764705882, alpha: 1)
        pressButtonLabel.font = UIFont(name: "SFProDisplay-Regular", size: 18)
        
        pressButtonLabel2.text = "to start massage"
        pressButtonLabel2.textColor = #colorLiteral(red: 0.7333333333, green: 0.337254902, blue: 0.3764705882, alpha: 1)
        pressButtonLabel2.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        
        turnOnButton.setImage(#imageLiteral(resourceName: "turnOnButton"), for: .normal)
        turnOnButton.setTitle("", for: .normal)
        
        vibroImage.image = #imageLiteral(resourceName: "noVibroBack")
        vibroImage2.image = #imageLiteral(resourceName: "noVibroBack")
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "SFProDisplay-Regular", size: 14) ?? UIFont.systemFont(ofSize: 14),
            .foregroundColor: #colorLiteral(red: 0.3921568627, green: 0.3921568627, blue: 0.3921568627, alpha: 1),
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        
         let attributeString = NSMutableAttributedString(
            string: "Doesn't vibrate?",
            attributes: attributes
        )
        
        doesntVibrateButton.setAttributedTitle(attributeString, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
