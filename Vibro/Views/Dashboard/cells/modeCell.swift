//
//  modeCell.swift
//  Vibro
//
//  Created by Контрабаев Артур on 16.09.2021.
//

import UIKit

class modeCell: UICollectionViewCell {

    @IBOutlet weak var modeImage: UIImageView!
    @IBOutlet weak var modeNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        backgroundColor = K.Colors.blackStandard
    
        modeNameLabel.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        modeNameLabel.textColor = #colorLiteral(red: 0.7333333333, green: 0.337254902, blue: 0.3764705882, alpha: 1)
        modeNameLabel.backgroundColor = K.Colors.blackStandard
        modeImage.backgroundColor = K.Colors.blackStandard
    }
    
    func load(name:String, image:UIImage){
        modeNameLabel.text = name
        modeImage.image = image
    }
    
    func selected() {
        modeImage.layer.borderWidth = 3

        modeImage.layer.cornerRadius = modeImage.layer.frame.width/2
        modeImage.layer.borderColor = K.Colors.pinkStandard.cgColor
    }
    
    func deselected(){
        modeImage.layer.borderWidth = 0
    }
}
