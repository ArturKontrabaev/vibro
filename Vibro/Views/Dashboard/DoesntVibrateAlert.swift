//
//  DoesntVibrateAlert.swift
//  Vibro
//
//  Created by Контрабаев Артур on 28.09.2021.
//

import UIKit

class DoesntVibrateAlert: UIViewController {
    
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var doesntVibrateLabel: UILabel!
    @IBOutlet weak var msg1: UILabel!
    @IBOutlet weak var msg2: UILabel!
    @IBOutlet weak var gotItButton: UIButton!
    
    @IBAction func gotItButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .black.withAlphaComponent(0.6)
        
        alertView.backgroundColor = #colorLiteral(red: 0.1607843137, green: 0.1607843137, blue: 0.1607843137, alpha: 1).withAlphaComponent(0.8)
        alertView.layer.borderWidth = 1
        alertView.layer.cornerRadius = 7
        alertView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
        doesntVibrateLabel.text = "Doesn’t vibrate?"
        doesntVibrateLabel.textColor = .white
        doesntVibrateLabel.font = UIFont(name: "SFProDisplay-Regular", size: 18)
        
    
        msg1.text = "Make sure that vibration are enabled in your device settings:"
        msg2.text = "Settings -> Sounds -> Switch on Vibrate on Silent or on Normal Mode"
  
        messageStyle(msg1)
        messageStyle(msg2)
     
        gotItButton.setTitle("Got it", for: .normal)
        gotItButton.layer.masksToBounds = true
        gotItButton.layer.cornerRadius = 12
        gotItButton.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0.253893137, alpha: 1)
        gotItButton.titleLabel?.font = UIFont(name: "SFProDisplay-Regular", size: 18)
        gotItButton.setTitleColor(.white, for: .normal)
    }
    
    func messageStyle(_ message:UILabel){
        message.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        message.textColor = .white
    }
}
