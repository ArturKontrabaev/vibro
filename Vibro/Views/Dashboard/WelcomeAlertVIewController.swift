//
//  welcomeAlert.swift
//  Vibro
//
//  Created by Контрабаев Артур on 22.09.2021.
//

import UIKit

class WelcomeAlertViewController: UIViewController {
    
    @IBOutlet weak var parentView: UIView!
    @IBOutlet weak var alertView: UIView!
    
    @IBOutlet weak var welcomeMessageLabel: UILabel!
    @IBOutlet weak var img1: UIImageView!
    @IBOutlet weak var img2: UIImageView!
    @IBOutlet weak var img3: UIImageView!

    @IBOutlet weak var msg1: UILabel!
    @IBOutlet weak var msg2: UILabel!
    @IBOutlet weak var msg3: UILabel!
    
    @IBOutlet weak var okButton: UIButton!
    
    @IBAction func okButtonPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        
        view.backgroundColor = .black.withAlphaComponent(0.6)
        
        alertView.backgroundColor = #colorLiteral(red: 0.1607843137, green: 0.1607843137, blue: 0.1607843137, alpha: 1).withAlphaComponent(0.8)
        alertView.layer.borderWidth = 1
        alertView.layer.cornerRadius = 7
        alertView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            
        welcomeMessageLabel.text = "Welcome to Massager!"
        welcomeMessageLabel.textColor = .white
        welcomeMessageLabel.font = UIFont(name: "SFProDisplay-Regular", size: 18)
        
        img1.image = #imageLiteral(resourceName: "tick")
        img2.image = #imageLiteral(resourceName: "tick")
        img3.image = #imageLiteral(resourceName: "tick")
    
        msg1.text = "The main function of this application is to use the vibration of your phone."
        msg2.text = "We are not responsible for a wrong usage of the app."
        msg3.text = "Vibration strength depends exclusively on the phone model."
        messageStyle(msg1)
        messageStyle(msg2)
        messageStyle(msg3)

        okButton.setTitle("OK", for: .normal)
        okButton.layer.masksToBounds = true
        okButton.layer.cornerRadius = 12
        okButton.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0.253893137, alpha: 1)
        okButton.titleLabel?.font = UIFont(name: "SFProDisplay-Regular", size: 18)
        okButton.setTitleColor(.white, for: .normal)
    }
    
    func messageStyle(_ message:UILabel){
        message.font = UIFont(name: "SFProDisplay-Regular", size: 14)
        message.textColor = .white
    }
}
