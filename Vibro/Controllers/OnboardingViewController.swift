//
//  OnboardingViewController.swift
//  Vibro
//
//  Created by Контрабаев Артур on 14.09.2021.
//

import UIKit
import SwiftVideoBackground
import SafariServices

class OnboardingViewController: UIViewController {
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var welcomeMessageLabel: UILabel!
    @IBOutlet weak var enableVirationLabel: UILabel!
    @IBOutlet weak var enableVibrationSwitch: UISwitch!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var termsButton: UIButton!
    @IBOutlet weak var andLabel: UILabel!
    @IBOutlet weak var privacyButton: UIButton!
    
    @IBOutlet weak var enableVibrationStackView: UIStackView!
    
    var currentScreen = 1
    let defaults = UserDefaults.standard
    
    @IBAction func termsButtonPressed(_ sender: UIButton) {
        openUrl(K.termsLink)
    }
    
    @IBAction func privacyButtonPressed(_ sender: UIButton) {
        openUrl(K.policyLink)
    }
    @IBAction func continueButtonPressed(_ sender: UIButton) {
        welcomeLabel.fadeTransition(0.7)
        enableVirationLabel.fadeTransition(0.7)
        welcomeMessageLabel.fadeTransition(0.7)
        switch currentScreen {
        case 1:
            currentScreen += 1
            playVideo(1)
            welcomeLabel.text = "High speed and intensity!"
            welcomeMessageLabel.text = "Find your ideal speed and intensity! Try different options to find your perfect massager settings. Highest speed and intensity guaranteed."
            enableVirationLabel.text = "Enable high speed & intensity"
        case 2:
            currentScreen += 1
            playVideo(2)
            welcomeLabel.text = "Try all modes and tools!"
            welcomeMessageLabel.text = "Try 3 days free trial, then $8.99/a month. Full access to all vibration modes, highest speed, maximum intensity without ads. Cancel anytime."
            enableVirationLabel.text = "Enable free trial"
        case 3:
            
            //MARK: - remove when payments implemented
            self.defaults.set(true, forKey: "isFirstLaunch")
            self.dismiss(animated: true) {
                let welcomeAlertVC = WelcomeAlertViewController.loadFromNib()
                welcomeAlertVC.modalPresentationStyle = .overFullScreen
                UIApplication.topViewController()?.present(welcomeAlertVC, animated: true, completion: nil)
            }
            
            //MARK: - remove when payments implemented
           
            let alert = UIAlertController(title: "Skip Free Trial?",
                                          message: "Free trial available only for the first app launch. Mostly of our users start with free trial and decide pay or not to pay later.",
                                          preferredStyle: .alert)
            
            let startTrialAction = UIAlertAction(title: "Start Free Trial", style: .default) { action in
            }
            
            let skipTrialAction = UIAlertAction(title: "Skip", style: .default) { action in
                
                self.defaults.set(true, forKey: "isFirstLaunch")
                self.dismiss(animated: true) {
                    let welcomeAlertVC = WelcomeAlertViewController.loadFromNib()
                    welcomeAlertVC.modalPresentationStyle = .overFullScreen
                    UIApplication.topViewController()?.present(welcomeAlertVC, animated: true, completion: nil)
                }
            }
            
            alert.addAction(startTrialAction)
            alert.addAction(skipTrialAction)
            
            //removed alert before payments impementation
            //present(alert, animated: true, completion: nil)
        
        default:
            currentScreen = 1
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureLayout()
        
    }
    
    func configureLayout() {
        
        playVideo(0)
        welcomeLabel.text = "Welcome to Massager!"
        welcomeLabel.font = UIFont(name: "SFProDisplay-Bold", size: 28)
        welcomeLabel.textColor = .white
        
        welcomeMessageLabel.text = "Vibration massage on your phone!  Having joy and relaxing has never been easier. Find your mode from the 12 suggested. Let’s start!"
        welcomeMessageLabel.font = UIFont(name: "SF-Pro-Text-Regular", size: 18)
        welcomeMessageLabel.textColor = .white.withAlphaComponent(0.7)
        
        enableVirationLabel.text = "Enable vibration"
        enableVirationLabel.textColor = .white
        enableVirationLabel.font = UIFont(name: "Roboto-Regular", size: 14)
        
        enableVibrationSwitch.onTintColor = .white.withAlphaComponent(0.7)
        
        continueButton.setTitle("Continue", for: .normal)
        continueButton.layer.masksToBounds = true
        continueButton.layer.cornerRadius = 12
        continueButton.backgroundColor = #colorLiteral(red: 1, green: 0, blue: 0.253893137, alpha: 1)
        continueButton.titleLabel?.font = UIFont(name: "SFProDisplay-Semibold", size: 18)
        continueButton.setTitleColor(.white, for: .normal)
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "Roboto-Regular", size: 14) ?? UIFont.systemFont(ofSize: 14),
            .foregroundColor: UIColor.systemGray,
            .underlineStyle: NSUnderlineStyle.single.rawValue
        ]
        var attributeString = NSMutableAttributedString(
            string: "Terms of Service",
            attributes: attributes
        )
        
        termsButton.setAttributedTitle(attributeString, for: .normal)
        
        andLabel.text = " & "
        andLabel.textColor = .systemGray
        andLabel.font = UIFont(name: "Roboto-Regular", size: 14)
        
        attributeString = NSMutableAttributedString(
            string: "Privacy Policy",
            attributes: attributes
        )
        
        privacyButton.setAttributedTitle(attributeString, for: .normal)
        
        
        enableVibrationStackView.layoutMargins = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        enableVibrationStackView.isLayoutMarginsRelativeArrangement = true
        enableVibrationStackView.layer.borderWidth = 1
        enableVibrationStackView.layer.cornerRadius = 26
        enableVibrationStackView.layer.borderColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        
    }
    
    func playVideo(_ file: Int) {
        try? VideoBackground.shared.play(
            view: view,
            videoName: "onb\(file)",
            videoType: "mp4",
            isMuted: true,
            darkness: 0.6,
            willLoopVideo: true,
            setAudioSessionAmbient: true
        )
    }
    
    func openUrl(_ link:String){
        if let url = URL(string: link) {
            let config = SFSafariViewController.Configuration()
            config.entersReaderIfAvailable = true
            
            let vc = SFSafariViewController(url: url, configuration: config)
            present(vc, animated: true)
        }
    }
}

extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
                                                            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
}

