//
//  DashboardViewController.swift
//  Vibro
//
//  Created by Контрабаев Артур on 16.09.2021.
//

import UIKit

class DashboardViewController: UIViewController, UITableViewDelegate, turnOnVibroCellDelegate {
    
    @IBOutlet weak var settingsBarButton: UIBarButtonItem!
    @IBOutlet var dashboardTableView: UITableView!
    
    public var chosenIntensity = K.startIntensity
    public var chosenSpeed =  K.startSpeed
    public var chosenMode = UserDefaults.standard.string(forKey: "chosenMode") ?? K.startMode

    var vibrationON: Bool = false
    
    var vibrator: Vibrator!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        vibrator = Vibrator(viewcontroller: self)
        
        Launch()
      
        dashboardTableView.register(cellClass: turnOnVibroCell.self)
        dashboardTableView.register(cellClass: intensityCell.self)
        dashboardTableView.register(cellClass: speedCell.self)
        dashboardTableView.register(cellClass: modesCell.self)
        
        dashboardTableView.dataSource = self
        dashboardTableView.delegate = self
        dashboardTableView.backgroundColor = K.Colors.blackStandard
        dashboardTableView.separatorStyle = .none
    }
    
    override func viewWillAppear(_ animated: Bool) {
        configureLayout ()
    }
    
    
    @IBAction func settingsBarButtonPressed(_ sender: UIBarButtonItem) {
        let settingsVC = (storyboard?.instantiateViewController(identifier: "settingsVC"))!
        navigationItem.backBarButtonItem = UIBarButtonItem(
            title: "", style: .plain, target: nil, action: nil)
        navigationItem.backBarButtonItem?.tintColor = .white
        navigationController?.pushViewController(settingsVC, animated: true)
    }
    
    
    func Launch(){
        if !UserDefaults.standard.bool(forKey: "isFirstLaunch") {
            let onboardingVC = (storyboard?.instantiateViewController(identifier: "oboardingVC"))!
            onboardingVC.modalPresentationStyle = .overFullScreen
            present(onboardingVC, animated: true, completion: nil)
        }
    }
    
    func configureLayout (){
        
        navigationController?.navigationBar.barTintColor = K.Colors.blackStandard
        navigationController?.navigationBar.backgroundColor = K.Colors.blackStandard
        navigationController?.navigationBar.isTranslucent = false
        view.backgroundColor = K.Colors.blackStandard
        
        navigationController?.setStatusBar(backgroundColor: K.Colors.blackStandard)
        settingsBarButton.image = #imageLiteral(resourceName: "settingsButton")
        settingsBarButton.tintColor = .white
    }
}

extension UINavigationController {

    func setStatusBar(backgroundColor: UIColor) {
        let statusBarFrame: CGRect
        if #available(iOS 13.0, *) {
            statusBarFrame = view.window?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero
        } else {
            statusBarFrame = UIApplication.shared.statusBarFrame
        }
        let statusBarView = UIView(frame: statusBarFrame)
        statusBarView.backgroundColor = backgroundColor
        view.addSubview(statusBarView)
    }
}
//MARK: - UITable Delegate + Data Source
extension DashboardViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(ofType: turnOnVibroCell.self)
            cell.delegate = self
            cell.selectedModeLabel.text = chosenMode
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(ofType: intensityCell.self)
            cell.delegate = self
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(ofType: speedCell.self)
            cell.delegate = self
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(ofType: modesCell.self)
            cell.delegate = self
            return cell
        default:
            return UITableViewCell()
        }
        
    }
}

//MARK: - Cells Delegates
extension DashboardViewController: IntensityCellDelegate, SpeedCellDelegate, ModesCellDelegate {
    
    func setNewMode(selectedMode: String) {
        chosenMode = selectedMode
        UserDefaults.standard.set(chosenMode, forKey: "chosenMode")
        if vibrationON {
            vibrator.play()
        }
        
        //setting current mode label
        if let cell = dashboardTableView.cellForRow(at: [0,0]) as? turnOnVibroCell {
            cell.selectedModeLabel.text = chosenMode
        }
        
    }
    
    func setNewIntensity(selectedIntensity: K.Intensity) {
        chosenIntensity = selectedIntensity
        UserDefaults.standard.set(chosenIntensity.rawValue, forKey: "chosenIntensity")
        if vibrationON {
            vibrator.play()
        }
    }
    
    func setNewSpeed(selectedSpeed: Float) {
        chosenSpeed = selectedSpeed
        UserDefaults.standard.set(chosenSpeed, forKey: "chosenSpeed")
    }
    
    func turnOnButtonPressed(vibroTurnedOn: Bool) {
        vibrationON = vibroTurnedOn
        vibrationON ? vibrator.play() : vibrator.stop()
    }
}
