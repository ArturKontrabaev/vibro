//
//  this height is the thickness      }      CustomSlider.swift
//  Vibro
//
//  Created by Контрабаев Артур on 01.10.2021.
//

import UIKit

class CustomSlider: UISlider {

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
           let point = CGPoint(x: bounds.minX, y: bounds.midY)
           return CGRect(origin: point, size: CGSize(width: bounds.width, height: 6)) //this height is the thickness
       }
}
