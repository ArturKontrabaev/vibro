//
//  Vibrator.swift
//  Vibro
//
//  Created by Контрабаев Артур on 24.09.2021.
//

import UIKit
import CoreHaptics
import AVFoundation

class Vibrator: NSObject {
    private let screen: DashboardViewController!
    var currentWave: Int = 0
    var currentPlayingMode: String?
    
    var vibrations: [Int:Bool] = [:]
    var vibrationID = 0
    var vibrationON: Bool = false
    
    // Maintain a variable to check for Core Haptics compatibility on device.
    lazy var supportsHaptics: Bool = {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.supportsHaptics
    }()
    
    // A haptic engine manages the connection to the haptic server.
    var engine: CHHapticEngine?
    
    init(viewcontroller: DashboardViewController){
        self.screen = viewcontroller
        super.init()
        
        createEngine()
    }
    
    //MARK: - Haptics
    /// - Tag: CreateEngine
    func createEngine() {
        
        // Create and configure a haptic engine.
        do {
            // Associate the haptic engine with the default audio session
            // to ensure the correct behavior when playing audio-based haptics.
            let audioSession = AVAudioSession.sharedInstance()
            engine = try CHHapticEngine(audioSession: audioSession)
        } catch let error {
            print("Engine Creation Error: \(error)")
        }
        
        guard let engine = engine else {
            print("Failed to create engine!")
            return
        }
        
        // The stopped handler alerts you of engine stoppage due to external causes.
        engine.stoppedHandler = { reason in
            print("The engine stopped for reason: \(reason.rawValue)")
            switch reason {
            case .audioSessionInterrupt:
                print("Audio session interrupt")
            case .applicationSuspended:
                print("Application suspended")
            case .idleTimeout:
                print("Idle timeout")
            case .systemError:
                print("System error")
            case .notifyWhenFinished:
                print("Playback finished")
            case .gameControllerDisconnect:
                print("Controller disconnected.")
            case .engineDestroyed:
                print("Engine destroyed.")
            @unknown default:
                print("Unknown error")
            }
            
        }
        
        // The reset handler provides an opportunity for your app to restart the engine in case of failure.
        engine.resetHandler = {
            // Try restarting the engine.
            print("The engine reset --> Restarting now!")
            do {
                try self.engine?.start()
            } catch {
                print("Failed to restart the engine: \(error)")
            }
        }
    }
    
    //MARK: - Play/Stop vibrations
    func play(){
        stop()
        vibrations[vibrationID] = true
        
        playOnce(id: vibrationID, duration: 0)
        
        func playOnce(id:Int, duration:Double) {
            DispatchQueue.main.asyncAfter(deadline: .now() + duration) {
                if self.vibrations[id]! {
                    let patternToPLay = self.generateNextHaptic()
                    self.playHaptics(pattern: patternToPLay)
                    playOnce(id: id, duration: patternToPLay.duration)
                } else {
                    return
                }
            }
        }
    }
    
    func stop(){
        currentWave = 0
        vibrations[vibrationID] = false
        vibrationID += 1
        
        engine?.stop(completionHandler: nil)
    }
    
    //MARK: - Single CHHaptic event generator
    func generateNextHaptic() -> CHHapticPattern{
        
        let sharpness: Float = 0.7
        var intensity: Float = 1.0
        var pattern: CHHapticPattern?
        let selectedModePattern = K.modesPattern[screen.chosenMode]!
        var adjustedSelectedModePattern: [Double] = []
        var chhapticevents: [CHHapticEvent] = []
        
        switch screen.chosenIntensity {
        case K.Intensity.hard:
            intensity = 1
        case K.Intensity.medium:
            intensity = 0.8
        case K.Intensity.soft:
            intensity = 0.6
        }
        
        for length in selectedModePattern {
            adjustedSelectedModePattern.append(Double(length)/(200 * Double(screen.chosenSpeed)/2))
        }
        
        let chapticIntensityParameters = [
            CHHapticEventParameter(parameterID: .hapticSharpness, value: sharpness),
            CHHapticEventParameter(parameterID: .hapticIntensity, value: intensity),
        ]
        
        if currentPlayingMode == screen.chosenMode {
            if currentWave >= adjustedSelectedModePattern.count{
                currentWave = 0
            }
            
            let newEvent = CHHapticEvent(eventType: .hapticContinuous, parameters: chapticIntensityParameters, relativeTime: adjustedSelectedModePattern[currentWave], duration: adjustedSelectedModePattern[currentWave+1])
            chhapticevents.append(newEvent)
            
            currentWave += 2
            do {
                pattern = try CHHapticPattern(events: chhapticevents, parameters: [])
            } catch {
                print("Failed to create pattern: \(error.localizedDescription).")
            }
            
            return pattern!
        }
        else {
            currentPlayingMode = screen.chosenMode
            currentWave = 0
            
            let newEvent = CHHapticEvent(eventType: .hapticContinuous, parameters: chapticIntensityParameters, relativeTime: adjustedSelectedModePattern[currentWave], duration: adjustedSelectedModePattern[currentWave+1])
            chhapticevents.append(newEvent)
            
            currentWave += 2
            do {
                pattern = try CHHapticPattern(events: chhapticevents, parameters: [])
            } catch {
                print("Failed to create pattern: \(error.localizedDescription).")
            }
            return pattern!
        }
        
    }
    
    //MARK: - Play haptic pattern
    func playHaptics(pattern: CHHapticPattern) {
        
        // If the device doesn't support Core Haptics, abort.
        if !supportsHaptics {
            return
        }
        
        do {
            try engine?.start()
            let player = try engine?.makePlayer(with: pattern)
            try player?.start(atTime: 0)
        } catch {
            print("Failed to play pattern: \(error.localizedDescription).")
        }
    }
}


